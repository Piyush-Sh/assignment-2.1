//ES6
// Exercise 1
var double = function(value:number) {    
    return value * 2;
};
console.log(double(10));
//convert
let data = (x:number)=>{ return x*2 } //x*2
console.log(data(10))

// Exercise 2
var greet = function(name:any) {    
    if (name === undefined) { 
        name = "Max"; }    
        console.log("Hello, " + name);
};
greet(undefined);
greet("Anna");
//convert
let ex2 =(name:string="MAx")=>{
    console.log("hello, "+name )
}
ex2()
ex2("Anna")

// Exercise 3
var numbers = [-3, 33, 38, 5];
console.log(Math.min.apply(Math, numbers));

//convert
console.log(Math.min(...numbers))

// Exercise 4
var newArray = [55, 20];
Array.prototype.push.apply(newArray, numbers);
console.log(newArray);

//convert
let arr=[55,20]
let arr1=[-3,33,38,5]
arr=[...arr,...arr1];
console.log(arr);
// Exercise 5
var testResults = [3.89, 2.99, 1.38];
var result1 = testResults[0];
var result2 = testResults[1];
var result3 = testResults[2];
console.log(result1, result2, result3);
//convert
let [res1,res2,res3]=testResults;
console.log(res1,res2,res3);
console.log(...testResults)

// Exercise 6
var scientist = {firstName: "Will", experience: 12};
var firstName = scientist.firstName;
var experience = scientist.experience;
console.log(firstName, experience);

//convert
let sci={
    firstN:"Will",
    exp:12,
}

let {firstN,exp}=sci;
console.log(firstN,exp);

//module.ts
/*
class Car{
    name:string;
    acceleration:number=0;

    hook(){
        console.log("Toooooooooot!");   
    }
    constructor(name:string){
        this.name=name
    }
    accelerate(speed:number){
        this.acceleration=this.acceleration+speed;
    }
}
let obj = new Car("BMw");
obj.hook();
console.log(obj.acceleration);
obj.accelerate(10);
console.log(obj.acceleration);
*/
//2
/*
class BaseObject{
    width:number=0;
    length:number=0;

}
class Rectangle extends BaseObject{
    width:number=5;
    length:number=2;
    calcSize = () :number => this.width* this.length 
}
let obj= new Rectangle();
console.log(obj.calcSize());
*/

//3
/*
class Person{
    private name:string=" ";

    get nameIs():string{
        return this.name;
    }

    set nameIs(value:string){
        if(value.length>3){
            this.name=value;
        }
        else{
            this.name=" "
        }
        }
}
let obj = new Person();
console.log(obj.nameIs)
obj.nameIs="Ma";
console.log(obj.nameIs)
obj.nameIs="Maximilian";
console.log(obj.nameIs)
*/